# Program prekvalifikacija - 5. nedelja

### Domaći zadatak:
Domaći zadatak iz prve nedelje proširiti na sledeći način: 
- Kreirati responsivnu web stranicu sa slikama i video materijalima. 
- Koristiti tri prelomne tačke (breaking points).  
- Na širinama ekrana do 800px navigacija i cela struktura sajta treba da se promene.
- Obavezno korišćenje spoljašnjeg CSS fajla. Koristiti normalize.css i css minifier, kreirani sajt postaviti na internet.



